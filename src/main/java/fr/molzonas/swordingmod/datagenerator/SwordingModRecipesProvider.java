package fr.molzonas.swordingmod.datagenerator;

import fr.molzonas.swordingmod.item.SwordingItems;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricRecipeProvider;
import net.minecraft.data.server.recipe.CookingRecipeJsonBuilder;
import net.minecraft.data.server.recipe.RecipeJsonProvider;
import net.minecraft.data.server.recipe.ShapedRecipeJsonBuilder;
import net.minecraft.data.server.recipe.ShapelessRecipeJsonBuilder;
import net.minecraft.item.Items;
import net.minecraft.recipe.CookingRecipeSerializer;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.book.RecipeCategory;

import java.util.function.Consumer;

public class SwordingModRecipesProvider extends FabricRecipeProvider {
    public SwordingModRecipesProvider(FabricDataOutput generator) {
        super(generator);
    }

    @Override
    public void generate(Consumer<RecipeJsonProvider> exporter) {
        // Sinizine
        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, SwordingItems.SINIZINE_INGOT.INSTANCE)
                .input(SwordingItems.SINIZINE_DUST.INSTANCE, 4)
                .input(Items.WATER_BUCKET, 1)
                .input(Items.IRON_INGOT, 1)
                .input(Items.COAL, 3)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_DUST.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_DUST.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(Items.WATER_BUCKET), FabricRecipeProvider.conditionsFromItem(Items.WATER_BUCKET))
                .criterion(FabricRecipeProvider.hasItem(Items.IRON_INGOT), FabricRecipeProvider.conditionsFromItem(Items.IRON_INGOT))
                .criterion(FabricRecipeProvider.hasItem(Items.COAL), FabricRecipeProvider.conditionsFromItem(Items.COAL))
                .offerTo(exporter);

        // Bastodonite
        ShapelessRecipeJsonBuilder.create(RecipeCategory.FOOD, SwordingItems.CHEESE.INSTANCE, 3)
                .input(SwordingItems.RENNET.INSTANCE, 3)
                .input(Items.MILK_BUCKET, 1)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.RENNET.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.RENNET.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(Items.MILK_BUCKET), FabricRecipeProvider.conditionsFromItem(Items.MILK_BUCKET))
                .offerTo(exporter);

        CookingRecipeJsonBuilder.create(Ingredient.ofItems(SwordingItems.CHEESE.INSTANCE),
                RecipeCategory.FOOD,
                SwordingItems.BASTODONITE.INSTANCE,
                2,
                20 * 30, CookingRecipeSerializer.SMELTING)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.CHEESE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.CHEESE.INSTANCE))
                .offerTo(exporter, "recipes/cheese_smelting");

        CookingRecipeJsonBuilder.create(Ingredient.ofItems(SwordingItems.CHEESE.INSTANCE),
                        RecipeCategory.FOOD,
                        SwordingItems.BASTODONITE.INSTANCE,
                        1,
                        20 * 60, CookingRecipeSerializer.CAMPFIRE_COOKING)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.CHEESE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.CHEESE.INSTANCE))
                .offerTo(exporter, "recipes/cheese_campfire_cooking");

        CookingRecipeJsonBuilder.create(Ingredient.ofItems(SwordingItems.CHEESE.INSTANCE),
                        RecipeCategory.FOOD,
                        SwordingItems.BASTODONITE.INSTANCE,
                        3,
                        20 * 20, CookingRecipeSerializer.SMOKING)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.CHEESE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.CHEESE.INSTANCE))
                .offerTo(exporter, "recipes/cheese_smoking");

        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.BASTODONITE_PICKAXE.INSTANCE)
                .pattern("bbb").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.BASTODONITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.BASTODONITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.BASTODONITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.BASTODONITE_SWORD.INSTANCE)
                .pattern(" b ").pattern(" b ").pattern(" s ")
                .input('b', SwordingItems.BASTODONITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.BASTODONITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.BASTODONITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.BASTODONITE_AXE.INSTANCE)
                .pattern(" bb").pattern(" sb").pattern(" s ")
                .input('b', SwordingItems.BASTODONITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.BASTODONITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.BASTODONITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.BASTODONITE_HOE.INSTANCE)
                .pattern(" bb").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.BASTODONITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.BASTODONITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.BASTODONITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.BASTODONITE_SHOVEL.INSTANCE)
                .pattern(" b ").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.BASTODONITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.BASTODONITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.BASTODONITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);

        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.BASTODONITE_FLINT_AND_STEEL.INSTANCE)
                .pattern(" bb").pattern("sbb").pattern("ss ")
                .input('b', SwordingItems.BASTODONITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.BASTODONITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.BASTODONITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);

        // Ciolium
        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.CIOLIUM_PICKAXE.INSTANCE)
                .pattern("bbb").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.CIOLIUM.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.CIOLIUM.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.CIOLIUM.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.CIOLIUM_SWORD.INSTANCE)
                .pattern(" b ").pattern(" b ").pattern(" s ")
                .input('b', SwordingItems.CIOLIUM.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.CIOLIUM.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.CIOLIUM.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.CIOLIUM_AXE.INSTANCE)
                .pattern(" bb").pattern(" sb").pattern(" s ")
                .input('b', SwordingItems.CIOLIUM.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.CIOLIUM.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.CIOLIUM.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.CIOLIUM_HOE.INSTANCE)
                .pattern(" bb").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.CIOLIUM.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.CIOLIUM.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.CIOLIUM.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.CIOLIUM_SHOVEL.INSTANCE)
                .pattern(" b ").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.CIOLIUM.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.CIOLIUM.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.CIOLIUM.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        // Devolurite
        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.DEVOLURITE_PICKAXE.INSTANCE)
                .pattern("bbb").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.DEVOLURITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.DEVOLURITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.DEVOLURITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.DEVOLURITE_SWORD.INSTANCE)
                .pattern(" b ").pattern(" b ").pattern(" s ")
                .input('b', SwordingItems.DEVOLURITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.DEVOLURITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.DEVOLURITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.DEVOLURITE_AXE.INSTANCE)
                .pattern(" bb").pattern(" sb").pattern(" s ")
                .input('b', SwordingItems.DEVOLURITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.DEVOLURITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.DEVOLURITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.DEVOLURITE_HOE.INSTANCE)
                .pattern(" bb").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.DEVOLURITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.DEVOLURITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.DEVOLURITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.DEVOLURITE_SHOVEL.INSTANCE)
                .pattern(" b ").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.DEVOLURITE.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.DEVOLURITE.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.DEVOLURITE.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        // Kamelister
        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.KAMELISTER_PICKAXE.INSTANCE)
                .pattern("bbb").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.KAMELISTER.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.KAMELISTER.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.KAMELISTER.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.KAMELISTER_SWORD.INSTANCE)
                .pattern(" b ").pattern(" b ").pattern(" s ")
                .input('b', SwordingItems.KAMELISTER.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.KAMELISTER.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.KAMELISTER.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.KAMELISTER_AXE.INSTANCE)
                .pattern(" bb").pattern(" sb").pattern(" s ")
                .input('b', SwordingItems.KAMELISTER.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.KAMELISTER.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.KAMELISTER.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.KAMELISTER_HOE.INSTANCE)
                .pattern(" bb").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.KAMELISTER.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.KAMELISTER.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.KAMELISTER.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.KAMELISTER_SHOVEL.INSTANCE)
                .pattern(" b ").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.KAMELISTER.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.KAMELISTER.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.KAMELISTER.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        // Seraphon
        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.SERAPHON_PICKAXE.INSTANCE)
                .pattern("bbb").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.SERAPHON.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SERAPHON.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SERAPHON.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.SERAPHON_SWORD.INSTANCE)
                .pattern(" b ").pattern(" b ").pattern(" s ")
                .input('b', SwordingItems.SERAPHON.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SERAPHON.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SERAPHON.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.SERAPHON_AXE.INSTANCE)
                .pattern(" bb").pattern(" sb").pattern(" s ")
                .input('b', SwordingItems.SERAPHON.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SERAPHON.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SERAPHON.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.SERAPHON_HOE.INSTANCE)
                .pattern(" bb").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.SERAPHON.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SERAPHON.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SERAPHON.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/

        /*ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, SwordingItems.SERAPHON_SHOVEL.INSTANCE)
                .pattern(" b ").pattern(" s ").pattern(" s ")
                .input('b', SwordingItems.SERAPHON.INSTANCE)
                .input('s', SwordingItems.SINIZINE_INGOT.INSTANCE)
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SERAPHON.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SERAPHON.INSTANCE))
                .criterion(FabricRecipeProvider.hasItem(SwordingItems.SINIZINE_INGOT.INSTANCE), FabricRecipeProvider.conditionsFromItem(SwordingItems.SINIZINE_INGOT.INSTANCE))
                .offerTo(exporter);*/
    }
}
