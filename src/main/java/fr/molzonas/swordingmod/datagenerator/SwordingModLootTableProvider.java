package fr.molzonas.swordingmod.datagenerator;

import fr.molzonas.swordingmod.item.SwordingItems;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricBlockLootTableProvider;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.entry.ItemEntry;

public class SwordingModLootTableProvider extends FabricBlockLootTableProvider {
    public SwordingModLootTableProvider(FabricDataOutput generator) { super (generator); }

    @Override
    public void generate() {
        LootPool.Builder poolBuilder = LootPool.builder()
                .with(ItemEntry.builder(SwordingItems.DEVOLURITE.INSTANCE));
    }
}
