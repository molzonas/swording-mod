package fr.molzonas.swordingmod.block;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.ExperienceDroppingBlock;
import net.minecraft.block.enums.Instrument;
import net.minecraft.util.math.intprovider.UniformIntProvider;

public enum SwordingBlocks {

    SINIZINE_ORE("sinizine_ore", FabricBlockSettings.create().instrument(Instrument.BASS).strength(2f), 5, 8);

    public final Block INSTANCE;
    private final String id;
    private final FabricBlockSettings settings;

    SwordingBlocks(String id, FabricBlockSettings settings) {
        this.id = id;
        this.settings = settings;
        this.INSTANCE = new Block(settings);
    }

    SwordingBlocks(String id, FabricBlockSettings settings, int minExp, int maxExp) {
        this.id = id;
        this.settings = settings;
        this.INSTANCE = new ExperienceDroppingBlock(settings, UniformIntProvider.create(minExp, maxExp));
    }

    public String getId() {
        return this.id;
    }

    public FabricBlockSettings getSettings() {
        return this.settings;
    }

    public enum SwordingBlocksType {
        BLOCK, EXPERIENCE_BLOCK
    }
}
