package fr.molzonas.swordingmod.item.material.kamelister;

import fr.molzonas.swordingmod.item.material.ExtendedToolMaterials;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.util.Rarity;

public class KamelisterPickaxe extends PickaxeItem {
    public static KamelisterPickaxe INSTANCE = new KamelisterPickaxe(ExtendedToolMaterials.KAMELISTER, 1, -2.8f, new Settings().rarity(Rarity.RARE).fireproof());

    private KamelisterPickaxe(ToolMaterial material, int attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }
}
