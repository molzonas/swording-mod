package fr.molzonas.swordingmod.item.material.seraphon;

import fr.molzonas.swordingmod.item.material.ExtendedToolMaterials;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.util.Rarity;

public class SeraphonPickaxe extends PickaxeItem {
    public static SeraphonPickaxe INSTANCE = new SeraphonPickaxe(ExtendedToolMaterials.SERAPHON, 1, -2.8f, new Settings().rarity(Rarity.RARE).fireproof());

    private SeraphonPickaxe(ToolMaterial material, int attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }
}
