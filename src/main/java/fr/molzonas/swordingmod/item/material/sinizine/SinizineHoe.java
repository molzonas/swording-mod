package fr.molzonas.swordingmod.item.material.sinizine;

import fr.molzonas.swordingmod.item.material.ExtendedToolMaterials;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.util.Hand;
import net.minecraft.util.Rarity;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class SinizineHoe extends ToolItem {
    public static Item.Settings SETTINGS = new FabricItemSettings().rarity(Rarity.RARE).fireproof();
    public static SinizineHoe INSTANCE = new SinizineHoe(ExtendedToolMaterials.SINIZINE, SETTINGS);

    public SinizineHoe(ToolMaterial material, Item.Settings settings) {
        super(material, settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack i = user.getStackInHand(hand);
        if (world.isClient) {
            if (!user.getItemCooldownManager().isCoolingDown(this)) {

                user.getItemCooldownManager().set(this, 40);
                return TypedActionResult.success(i);
            }
            return TypedActionResult.fail(i);
        }
        return TypedActionResult.success(i);
    }
}
