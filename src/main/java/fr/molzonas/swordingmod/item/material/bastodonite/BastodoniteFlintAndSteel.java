package fr.molzonas.swordingmod.item.material.bastodonite;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Hand;
import net.minecraft.util.Rarity;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.UseAction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.RandomSeed;
import net.minecraft.world.World;

import java.util.Arrays;
import java.util.List;

public class BastodoniteFlintAndSteel extends Item {
    public static BastodoniteFlintAndSteel INSTANCE = new BastodoniteFlintAndSteel(new FabricItemSettings().maxCount(1).rarity(Rarity.UNCOMMON).maxDamage(1010));
    private final List<Block> birchPropaganda = Arrays.asList(
            Blocks.BIRCH_LOG,
            Blocks.BIRCH_BUTTON,
            Blocks.BIRCH_DOOR,
            Blocks.BIRCH_FENCE,
            Blocks.BIRCH_LEAVES,
            Blocks.BIRCH_PLANKS,
            Blocks.STRIPPED_BIRCH_LOG,
            Blocks.STRIPPED_BIRCH_WOOD,
            Blocks.BIRCH_WOOD,
            Blocks.BIRCH_STAIRS,
            Blocks.BIRCH_SAPLING,
            Blocks.BIRCH_SLAB);

    public BastodoniteFlintAndSteel(Settings settings) {
        super(settings);
    }


    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        if (user.getItemCooldownManager().isCoolingDown(this)) {
            return TypedActionResult.fail(new ItemStack(this));
        }

        user.setCurrentHand(hand);
        return TypedActionResult.consume(user.getStackInHand(hand));
    }

    @Override
    public ItemStack finishUsing(ItemStack stack, World world, LivingEntity user) {
        stack.damage(5, user, e -> e.sendToolBreakStatus(user.getActiveHand()));
        return super.finishUsing(stack, world, user);
    }

    @Override
    public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean selected) {
        if (selected && entity instanceof PlayerEntity pe) {
            if (pe.getItemUseTime() == 60) {
                if (!world.isClient) {
                    world.playSound(null, pe.getX(), pe.getY(), pe.getZ(), SoundEvents.BLOCK_NOTE_BLOCK_BELL, SoundCategory.PLAYERS, 0.5f, 1f, RandomSeed.getSeed());
                }
            }
        }
        super.inventoryTick(stack, world, entity, slot, selected);
    }

    @Override
    public void onStoppedUsing(ItemStack stack, World world, LivingEntity user, int remainingUseTicks) {
        if (!world.isClient && user instanceof PlayerEntity pe) {
            int usedDuration = this.getMaxUseTime(stack) - user.getItemUseTimeLeft();
            if (usedDuration >= 60) {
                activateEffect(pe, pe.getActiveHand(), world);
                stack.damage(5, user, e -> e.sendToolBreakStatus(user.getActiveHand()));
            }
        }
    }

    private void activateEffect(PlayerEntity user, Hand hand, World world) {
        BlockPos bp = user.getBlockPos();
        int radius = 15;

        ItemStack bk = user.getStackInHand(hand);
        bk.damage(5, user, e -> e.sendToolBreakStatus(hand));

        boolean somethingBurn = false;

        for (int x = -radius; x <= radius; x++) {
            for (int y = -radius; y <= radius; y++) {
                for (int z = -radius; z <= radius; z++) {
                    BlockPos blockPos = bp.add(x, y, z);
                    Block block = world.getBlockState(blockPos).getBlock();
                    if (birchPropaganda.contains(block)) {
                        BlockPos birchDetected = getNearestPosUp(world, blockPos);
                        if (world.getBlockState(birchDetected).isAir()) {
                            world.setBlockState(birchDetected, Blocks.FIRE.getDefaultState());
                            somethingBurn = true;
                        }
                    }
                }
            }
        }
        if (somethingBurn) {
            world.playSound(null, user.getX(), user.getY(), user.getZ(), SoundEvents.ENTITY_WITHER_SHOOT, SoundCategory.PLAYERS, 0.7f, 0.5f, RandomSeed.getSeed());
        } else {
            world.playSound(null, user.getX(), user.getY(), user.getZ(), SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.PLAYERS, 0.7f, 1.5f, RandomSeed.getSeed());
        }
        user.getItemCooldownManager().set(this, 20 * 10);
    }

    private BlockPos getNearestPosUp(World world, BlockPos pos) {
        BlockState bs = world.getBlockState(pos);
        if (bs.isAir() || !birchPropaganda.contains(bs.getBlock())) return pos;
        return getNearestPosUp(world, pos.up());
    }

    @Override
    public int getMaxUseTime(ItemStack stack) {
        return 20 * 300;
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.BOW;
    }
}
