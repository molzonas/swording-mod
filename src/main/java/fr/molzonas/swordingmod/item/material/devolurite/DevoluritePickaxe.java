package fr.molzonas.swordingmod.item.material.devolurite;

import fr.molzonas.swordingmod.item.material.ExtendedToolMaterials;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.util.Rarity;

public class DevoluritePickaxe extends PickaxeItem {
    public static DevoluritePickaxe INSTANCE = new DevoluritePickaxe(ExtendedToolMaterials.DEVOLURITE, 1, -2.8f, new Settings().rarity(Rarity.RARE).fireproof());

    private DevoluritePickaxe(ToolMaterial material, int attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }
}
