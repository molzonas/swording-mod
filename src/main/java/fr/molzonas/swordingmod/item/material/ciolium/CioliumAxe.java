package fr.molzonas.swordingmod.item.material.ciolium;

import fr.molzonas.swordingmod.item.material.ExtendedToolMaterials;
import net.minecraft.item.AxeItem;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.util.Rarity;

public class CioliumAxe extends AxeItem {
    public static CioliumAxe INSTANCE = new CioliumAxe(ExtendedToolMaterials.CIOLIUM, 7, -3f, new Settings().rarity(Rarity.RARE).fireproof());

    private CioliumAxe(ToolMaterial material, int attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }
}
