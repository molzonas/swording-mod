package fr.molzonas.swordingmod.item.material.bastodonite;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

import java.util.List;

public class BastodoniteConstants {
    public static TypedActionResult<ItemStack> use(PlayerEntity user, Hand hand) {
        ItemStack itemStack = user.getStackInHand(hand);
        ItemStack offhandStack = user.getOffHandStack();
        if (offhandStack.isEmpty() && user.canConsume(true)) {
            user.setCurrentHand(hand);
            return TypedActionResult.consume(itemStack);
        }
        return TypedActionResult.fail(itemStack);
    }

    public static ItemStack finishUsing(ItemStack itemStack, World world, LivingEntity user, List<StatusEffectInstance> effectsTop, List<StatusEffectInstance> effects2, List<StatusEffectInstance> effects3, List<StatusEffectInstance> effects4, List<StatusEffectInstance> effectsWorst) {
        int maxDurability = itemStack.getDamage();
        int durability = itemStack.getMaxDamage() - maxDurability;
        float pctDurability = ((float)durability / (float)maxDurability);

        itemStack.damage(itemStack.getMaxDamage(), user, (p) -> p.sendToolBreakStatus(user.getActiveHand()));

        if (!world.isClient) {
            if (pctDurability > 0.95f) { //if (durability > 1000) {
                for (StatusEffectInstance sei : effectsTop) {
                    user.addStatusEffect(sei);
                }
            } else if (pctDurability > 0.7f) { //} else if (durability > 700) {
                for (StatusEffectInstance sei : effects2) {
                    user.addStatusEffect(sei);
                }
            } else if (pctDurability > 0.5f) { //} else if (durability > 500) {
                for (StatusEffectInstance sei : effects3) {
                    user.addStatusEffect(sei);
                }
            } else if (pctDurability > 0.3f) { //} else if (durability > 300) {
                for (StatusEffectInstance sei : effects4) {
                    user.addStatusEffect(sei);
                }
            } else {
                for (StatusEffectInstance sei : effectsWorst) {
                    user.addStatusEffect(sei);
                }
            }
        }

        itemStack.decrement(1);

        return itemStack;
    }
}
