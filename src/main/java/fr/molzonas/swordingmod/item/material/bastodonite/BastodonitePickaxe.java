package fr.molzonas.swordingmod.item.material.bastodonite;

import fr.molzonas.swordingmod.item.material.ExtendedToolMaterials;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.util.Hand;
import net.minecraft.util.Rarity;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.UseAction;
import net.minecraft.world.World;

import java.util.List;

public class BastodonitePickaxe extends PickaxeItem {
    public static Settings SETTINGS = new FabricItemSettings().rarity(Rarity.RARE).fireproof();
    public static BastodonitePickaxe INSTANCE = new BastodonitePickaxe(ExtendedToolMaterials.BASTODONITE, 1, -2.8f, SETTINGS);

    private BastodonitePickaxe(ToolMaterial material, int attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        return BastodoniteConstants.use(user, hand);
    }

    @Override
    public ItemStack finishUsing(ItemStack itemStack, World world, LivingEntity user) {
        return BastodoniteConstants.finishUsing(itemStack, world, user,
                List.of(new StatusEffectInstance(StatusEffects.HASTE, 20 * 120, 1)),
                List.of(new StatusEffectInstance(StatusEffects.HASTE, 20 * 60, 0)),
                List.of(new StatusEffectInstance(StatusEffects.HASTE, 20 * 20, 0)),
                List.of(new StatusEffectInstance(StatusEffects.HASTE, 20 * 10, 0), new StatusEffectInstance(StatusEffects.NAUSEA, 20 * 30, 0)),
                List.of(new StatusEffectInstance(StatusEffects.NAUSEA, 20 * 30, 0), new StatusEffectInstance(StatusEffects.POISON, 20 * 10, 0)));
    }

    @Override
    public int getMaxUseTime(ItemStack stack) {
        return 40;
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.EAT;
    }

}
