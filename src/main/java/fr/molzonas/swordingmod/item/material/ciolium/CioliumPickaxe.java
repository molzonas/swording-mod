package fr.molzonas.swordingmod.item.material.ciolium;

import fr.molzonas.swordingmod.item.material.ExtendedToolMaterials;
import net.minecraft.item.Item;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.util.Rarity;

public class CioliumPickaxe extends PickaxeItem {
    public static CioliumPickaxe INSTANCE = new CioliumPickaxe(ExtendedToolMaterials.CIOLIUM, 1, -2.8f, new Item.Settings().rarity(Rarity.RARE).fireproof());

    private CioliumPickaxe(ToolMaterial material, int attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }
}
