package fr.molzonas.swordingmod.item.material;

import fr.molzonas.swordingmod.item.SwordingItems;
import net.minecraft.item.ToolMaterial;
import net.minecraft.recipe.Ingredient;

import java.util.function.Supplier;

public enum ExtendedToolMaterials implements ToolMaterial {
    SWORDING(4, 3305, 10, 5, 3, () -> Ingredient.ofItems(SwordingItems.SINIZINE_INGOT.INSTANCE)),
    BASTODONITE(1, 1096, 10, 3, 21, () -> Ingredient.ofItems(SwordingItems.BASTODONITE.INSTANCE)),
    CIOLIUM(3, 2255, 7, 2, 16, () -> Ingredient.ofItems(SwordingItems.CIOLIUM.INSTANCE)),
    DEVOLURITE(2, 1445, 6, 2, 28, () -> Ingredient.ofItems(SwordingItems.DEVOLURITE.INSTANCE)),
    KAMELISTER(0, 345, 18, 6, 11, () -> Ingredient.ofItems(SwordingItems.KAMELISTER.INSTANCE)),
    SERAPHON(4, 1865, 4, 3, 15, () -> Ingredient.ofItems(SwordingItems.SERAPHON.INSTANCE)),
    SINIZINE(4, 100, 24, 4, 5, () -> Ingredient.ofItems(SwordingItems.SINIZINE_INGOT.INSTANCE));

    private final int miningLevel;
    private final int itemDurability;
    private final float miningSpeed;
    private final float attackDamage;
    private final int enchantability;
    private final Supplier<Ingredient> repairIngredient;

    ExtendedToolMaterials(int miningLevel, int itemDurability, float miningSpeed, float attackDamage, int enchantability, Supplier<Ingredient> repairIngredient) {
        this.miningLevel = miningLevel;
        this.itemDurability = itemDurability;
        this.miningSpeed = miningSpeed;
        this.attackDamage = attackDamage;
        this.enchantability = enchantability;
        this.repairIngredient = repairIngredient;
    }

    @Override
    public int getDurability() {
        return this.itemDurability;
    }

    @Override
    public float getMiningSpeedMultiplier() {
        return this.miningSpeed;
    }

    @Override
    public float getAttackDamage() {
        return this.attackDamage;
    }

    @Override
    public int getMiningLevel() {
        return this.miningLevel;
    }

    @Override
    public int getEnchantability() {
        return this.enchantability;
    }

    @Override
    public Ingredient getRepairIngredient() {
        return this.repairIngredient.get();
    }
}
