package fr.molzonas.swordingmod.item;

import fr.molzonas.swordingmod.block.SwordingBlocks;
import fr.molzonas.swordingmod.item.material.bastodonite.*;
import fr.molzonas.swordingmod.item.material.ciolium.CioliumAxe;
import fr.molzonas.swordingmod.item.material.ciolium.CioliumPickaxe;
import fr.molzonas.swordingmod.item.material.devolurite.DevoluritePickaxe;
import fr.molzonas.swordingmod.item.material.kamelister.KamelisterPickaxe;
import fr.molzonas.swordingmod.item.material.seraphon.SeraphonPickaxe;
import fr.molzonas.swordingmod.item.material.sinizine.SinizineHoe;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.block.Block;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.BlockItem;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.util.Rarity;

public enum SwordingItems {
    // Sinizine
    SINIZINE_INGOT("sinizine_ingot", new FabricItemSettings().rarity(Rarity.UNCOMMON).maxCount(64).fireproof()),
    SINIZINE_ORE("sinizine_ore", new FabricItemSettings().rarity(Rarity.UNCOMMON).maxCount(64).fireproof(), SwordingBlocks.SINIZINE_ORE.INSTANCE),
    SINIZINE_DUST("sinizine_dust", new FabricItemSettings().rarity(Rarity.UNCOMMON).maxCount(64)),
    SINIZINE_HOE("sinizine_hoe", SinizineHoe.INSTANCE),

    // Bastodonite
    BASTODONITE("bastodonite", new FabricItemSettings().rarity(Rarity.UNCOMMON).maxCount(64).fireproof().food(new FoodComponent.Builder().snack().hunger(2).statusEffect(new StatusEffectInstance(StatusEffects.RESISTANCE, 20 * 30, 0), 0.2f).build())),
    BASTODONITE_AXE("bastodonite_axe", BastodoniteAxe.INSTANCE),
    BASTODONITE_PICKAXE("bastodonite_pickaxe", BastodonitePickaxe.INSTANCE),
    BASTODONITE_SHOVEL("bastodonite_shovel", BastodoniteShovel.INSTANCE),
    BASTODONITE_HOE("bastodonite_hoe", BastodoniteHoe.INSTANCE),
    BASTODONITE_SWORD("bastodonite_sword", BastodoniteSword.INSTANCE),
    BASTODONITE_FLINT_AND_STEEL("bastodonite_flint_and_steel", BastodoniteFlintAndSteel.INSTANCE),
    RENNET("rennet", new FabricItemSettings().rarity(Rarity.COMMON).maxCount(64).food(new FoodComponent.Builder().alwaysEdible().statusEffect(new StatusEffectInstance(StatusEffects.POISON, 20 * 3, 3), 1).build())),
    CHEESE("cheese", new FabricItemSettings().rarity(Rarity.COMMON).maxCount(64).food(new FoodComponent.Builder().hunger(4).saturationModifier(2).meat().build())),

    // Ciolium
    CIOLIUM("ciolium", new FabricItemSettings().maxCount(64).rarity(Rarity.UNCOMMON)),
    CIOLIUM_AXE("ciolium_axe", CioliumAxe.INSTANCE),
    CIOLIUM_PICKAXE("ciolium_pickaxe", CioliumPickaxe.INSTANCE),
    //CIOLIUM_SHOVEL("bastodonite_shovel", new FabricItemSettings()),
    //CIOLIUM_HOE("bastodonite_hoe", new FabricItemSettings()),
    //CIOLIUM_SWORD("bastodonite_sword", new FabricItemSettings()),

    // Devolurite
    DEVOLURITE("devolurite", new FabricItemSettings().maxCount(64).rarity(Rarity.UNCOMMON)),
    //DEVOLURITE_AXE("bastodonite_axe", new FabricItemSettings()),
    DEVOLURITE_PICKAXE("devolurite_pickaxe", DevoluritePickaxe.INSTANCE),
    //DEVOLURITE_SHOVEL("bastodonite_shovel", new FabricItemSettings()),
    //DEVOLURITE_HOE("bastodonite_hoe", new FabricItemSettings()),
    //DEVOLURITE_SWORD("bastodonite_sword", new FabricItemSettings()),

    // Kamelister
    KAMELISTER("kamelister", new FabricItemSettings().maxCount(64).rarity(Rarity.UNCOMMON)),
    //KAMELISTER_AXE("bastodonite_axe", new FabricItemSettings()),
    KAMELISTER_PICKAXE("kamelister_pickaxe", KamelisterPickaxe.INSTANCE),
    //KAMELISTER_SHOVEL("bastodonite_shovel", new FabricItemSettings()),
    //KAMELISTER_HOE("bastodonite_hoe", new FabricItemSettings()),
    //KAMELISTER_SWORD("bastodonite_sword", new FabricItemSettings()),

    // Seraphon
    SERAPHON("seraphon", new FabricItemSettings().maxCount(64).rarity(Rarity.UNCOMMON)),
    //SERAPHON_AXE("bastodonite_axe", new FabricItemSettings()),
    SERAPHON_PICKAXE("seraphon_pickaxe", SeraphonPickaxe.INSTANCE);
    //SERAPHON_SHOVEL("bastodonite_shovel", new FabricItemSettings()),
    //SERAPHON_HOE("bastodonite_hoe", new FabricItemSettings()),
    //SERAPHON_SWORD("bastodonite_sword", new FabricItemSettings());

    public final Item INSTANCE;
    private final String id;
    private final FabricItemSettings settings;

    SwordingItems(String id, FabricItemSettings settings) {
        this.id = id;
        this.settings = settings;
        this.INSTANCE = new Item(settings);
    }

    SwordingItems(String id, FabricItemSettings settings, Block linkedBlock) {
        this.id = id;
        this.settings = settings;
        this.INSTANCE = new BlockItem(linkedBlock, settings);
    }

    SwordingItems(String id, Item item) {
        this.id = id;
        this.settings = null;
        this.INSTANCE = item;
    }

    public String getId() {
        return this.id;
    }

    public FabricItemSettings getSettings() {
        return this.settings;
    }
}
