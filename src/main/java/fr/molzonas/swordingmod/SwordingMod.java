package fr.molzonas.swordingmod;

import fr.molzonas.swordingmod.block.SwordingBlocks;
import fr.molzonas.swordingmod.item.SwordingItems;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.loot.v2.LootTableEvents;
import net.minecraft.block.Blocks;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.CowEntity;
import net.minecraft.item.*;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.condition.KilledByPlayerLootCondition;
import net.minecraft.loot.condition.MatchToolLootCondition;
import net.minecraft.loot.condition.RandomChanceLootCondition;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.provider.number.ConstantLootNumberProvider;
import net.minecraft.loot.provider.number.UniformLootNumberProvider;
import net.minecraft.predicate.NumberRange;
import net.minecraft.predicate.item.ItemPredicate;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.world.EntityList;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.feature.PlacedFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SwordingMod implements ModInitializer {
	public static final String MOD_NAMESPACE = "swording-mod";
	private static final Identifier BOOKSHELF_LOOTTABLE_ID = Blocks.BOOKSHELF.getLootTableId();
	private static final Identifier COW_LOOTTABLE_ID =new Identifier("minecraft", "entities/cow");

	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod id as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
	public static final Logger LOGGER = LoggerFactory.getLogger(MOD_NAMESPACE);
	public static final RegistryKey<PlacedFeature> SINIZINE_ORE_GEN = RegistryKey.of(RegistryKeys.PLACED_FEATURE, new Identifier(MOD_NAMESPACE, "sinizine_ore"));

	@Override
	public void onInitialize() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.
		registerBlocks();
		registerItems();

		final ItemGroup SWORDING_MOD_ITEM_GROUP = FabricItemGroup.builder()
				.icon(() -> new ItemStack(SwordingItems.SINIZINE_DUST.INSTANCE))
				.displayName(Text.translatable("itemGroup.swording-mod.swording_group"))
				.entries((context, entries) -> {
					for (SwordingItems value : SwordingItems.values()) {
						entries.add(value.INSTANCE);
					}
				}).build();

		Registry.register(Registries.ITEM_GROUP, new Identifier(MOD_NAMESPACE, "swording_group"), SWORDING_MOD_ITEM_GROUP);

		BiomeModifications.addFeature(BiomeSelectors.foundInOverworld(), GenerationStep.Feature.UNDERGROUND_ORES, SINIZINE_ORE_GEN);

		LootTableEvents.MODIFY.register((resourceManager, lootManager, id, tableBuilder, source) -> {
			if (source.isBuiltin() && BOOKSHELF_LOOTTABLE_ID.equals(id)) {
				LootPool.Builder b = LootPool.builder()
						.rolls(ConstantLootNumberProvider.create(1))
						.with(ItemEntry.builder(SwordingItems.DEVOLURITE.INSTANCE))
						.conditionally(RandomChanceLootCondition.builder(0.1f));
				tableBuilder.pool(b);
			}
			if (COW_LOOTTABLE_ID.equals(id)) {
				LootPool b = LootPool.builder()
						.rolls(ConstantLootNumberProvider.create(1))
						.with(ItemEntry.builder(SwordingItems.RENNET.INSTANCE))
						.conditionally(RandomChanceLootCondition.builder(0.15f))
						/*.conditionally(MatchToolLootCondition.builder(ItemPredicate.Builder.create()
								.items(Items.WOODEN_AXE,
										Items.STONE_AXE,
										Items.IRON_AXE,
										Items.GOLDEN_AXE,
										Items.DIAMOND_AXE,
										Items.NETHERITE_AXE,
										SwordingItems.BASTODONITE_AXE.INSTANCE)
								.durability(NumberRange.IntRange.ANY)))*/
						.build();
				tableBuilder.pool(b);
			}
		});

		LOGGER.info("Swording Mod loaded");
	}

	private void registerItems() {
		for (SwordingItems i : SwordingItems.values()) {
			Registry.register(Registries.ITEM, new Identifier(MOD_NAMESPACE, i.getId()), i.INSTANCE);
		}
	}

	private void registerBlocks() {
		for (SwordingBlocks b : SwordingBlocks.values()) {
			Registry.register(Registries.BLOCK, new Identifier(MOD_NAMESPACE, b.getId()), b.INSTANCE);
		}
	}
}